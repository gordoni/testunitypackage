using UnityEngine;

namespace JellyTools.AtoB
{
    internal class AnimationParameter
    {
        public MotionData MotionData { get; }
        public Transform LookAtTarget { get; }

        public bool HasLookAtTarget { get; }

        public AnimationParameters(MotionData motionData)
        {
            MotionData = motionData;
        }

        public AnimationParameters(MotionData motionData, Transform lookAtTarget) : this(motionData)
        {
            if (lookAtTarget != null)
            {
                LookAtTarget = lookAtTarget;
                HasLookAtTarget = true;
            }
        }
    }
}
